import numpy as np
import pandas as pd
import statsmodels.api as sm

df = pd.read_csv('ufc-fighters-statistics.csv')
x = df.drop(['win_ratio', 'name', 'nickname', 'stance', 'date_of_birth', 'weight_class', 'wins', 'losses', 'draws'], axis=1)
y = df['win_ratio']

x = sm.add_constant(x)
model = sm.OLS(y, x)
results = model.fit()

print(results.summary())